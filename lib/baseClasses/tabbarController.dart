import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tele_medi/screens/appointmentsScreen.dart';
import 'package:tele_medi/screens/clinicsSreen.dart';
import 'package:tele_medi/screens/homeScreen.dart';
import 'package:tele_medi/screens/profileScreen.dart';
import 'package:tele_medi/screens/settingsScreen.dart';

class TabScreen extends StatefulWidget {
  static const routeName = "tabbarController";

  @override
  State<StatefulWidget> createState() {
    return _TabScreenState();
  }
}

class _TabScreenState extends State<TabScreen> {
  List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    super.initState();
    this._pages = [
      {"title": "Home", "page": HomeScreen()},
      {"title": "My Appointments", "page": MyAppointments()},
      {"title": "My Clinics", "page": MyClinics()},
      {"title": "Profile", "page": ProfileScreen()}
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(this._pages[this._selectedPageIndex]["title"]),
            automaticallyImplyLeading: false),
        body: this._pages[this._selectedPageIndex]["page"],
        bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Theme.of(context).primaryColor,
            unselectedItemColor: Theme.of(context).accentColor,
            selectedItemColor: Colors.white,
            currentIndex: _selectedPageIndex,
            onTap: _selectedPage,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.home,
                      color: this.isSelected(this._selectedPageIndex == 0)[0]),
                  title: Text("Home",
                      style: this.isSelected(this._selectedPageIndex == 0)[1])),
              BottomNavigationBarItem(
                  icon: Icon(Icons.theaters,
                      color: this.isSelected(this._selectedPageIndex == 1)[0]),
                  title: Text("My Appointments",
                      style: this.isSelected(this._selectedPageIndex == 1)[1])),
              BottomNavigationBarItem(
                  icon: Icon(Icons.star,
                      color: this.isSelected(this._selectedPageIndex == 2)[0]),
                  title: Text("My Clinics",
                      style: this.isSelected(this._selectedPageIndex == 2)[1])),
              BottomNavigationBarItem(
                  icon: Icon(Icons.swap_vert,
                      color: this.isSelected(this._selectedPageIndex == 3)[0]),
                  title: Text("Profile",
                      style: this.isSelected(this._selectedPageIndex == 3)[1]))
            ]));
  }

  void _selectedPage(int index) {
    setState(() {
      this._selectedPageIndex = index;
    });
  }

  List isSelected(bool isSelected) {
    return [
      isSelected ? Theme.of(context).primaryColor : Colors.grey,
      TextStyle(
          color: isSelected ? Theme.of(context).primaryColor : Colors.grey)
    ];
  }
}
