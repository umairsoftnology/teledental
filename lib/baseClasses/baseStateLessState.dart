import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class BaseStatelessWidget extends StatelessWidget {
  BuildContext ctx;

  @override
  Widget build(BuildContext context) {
    this.ctx = context;
    return Scaffold(
        appBar: appBar(),
        floatingActionButton: fab(),
        body: Container(
          child: body(context),
//          color: Colors.amber,
        ));
  }

  Widget body(BuildContext context);

  Widget fab() {
    return Container();
  }

  Widget appBar() {
    return null;
  }
}
