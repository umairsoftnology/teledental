import 'dart:async';

import 'package:tele_medi/screens/videoCall.dart';
import 'package:tele_medi/services/callProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quickblox_sdk/models/qb_rtc_session.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';

/*
abstract class _BaseStatefulState<T extends StatefulWidget> extends State<T> {

  _BaseStatefulState() {
    // Parent constructor
  }

  void baseMethod() {
    // Parent method
  }
}
*/

abstract class BaseStatefulWidget extends StatefulWidget {
  BaseStatefulWidget({Key key}) : super(key: key);
}

abstract class BaseStatefulWidgetState<Page extends BaseStatefulWidget>
    extends State<Page> {
  String screenName();

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
}

mixin BasicPageMixin<Page extends BaseStatefulWidget>
    on BaseStatefulWidgetState<Page> {
  Widget body(
      BuildContext context); // must implement if you own BasicPage mixin

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: appBar_(),
        floatingActionButton: fab(),
        body: Container(
          child: body(context),
        ));
  }

  Widget fab() {
    // override it if you make a floating action button
    return Container();
  }

  Widget appBar_() {
    // override it, if you want to make an App Bar
    return null;
  }

  @override
  void dispose() {
    super.dispose();
  }
}

//TODO: VideoCallMixin
mixin VideoCallMixin<Page extends BaseStatefulWidget>
    on BaseStatefulWidgetState<Page> {
  StreamSubscription _streamSubscription;

  Widget body(); // must implement if you own VideoCallMixin mixin

  @override
  void initState() {
    super.initState();

    _streamSubscription = VideoCallManager().stream.listen((event) async {
      print("event : $event");
      try {
        // Map<String, Object> userInfo = new Map();

        QBRTCSession qbSession = event;
//        QBRTCSession obj = await QB.webrtc.accept(qbSession.id, userInfo: userInfo);
        print("this._startVideoCallScreen");

        showDialog(
            context: this.scaffoldKey.currentContext,
            builder: (context) => AlertDialog(
                    title: Text("${qbSession.initiatorId} is calling you."),
                    content: Text("Do you want to receive?"),
                    actions: [
                      FlatButton(
                          onPressed: () async {
                            // Navigator.of(context).pop();
                            VideoCallManager()
                                .acceptCall(qbSession.id, context)
                                .then((value) {
                              this._startVideoCallScreen(
                                  value.id, value.initiatorId, context);
                            });
                          },
                          child: Text("Yes")),
                      FlatButton(
                          onPressed: () async {
                            // Navigator.of(context).pop();
                            VideoCallManager().rejectCall(qbSession.id);
                          },
                          child: Text("No"))
                    ]));
      } on PlatformException catch (e) {
        print("Call accept exception : ${e.toString()}");
      }
    }, onDone: () {
      print("Done");
    }, cancelOnError: false);
  }

  @override
  void dispose() {
    this._streamSubscription.cancel();
    print("VideoCallMixin dispose called");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(screenName()),
        ),
        floatingActionButton: fab(),
        body: Container(
          child: body(),
//          color: Colors.amber,
        ));
  }

  Widget fab() {
    // override it if you make a floating action button
    return Container();
  }

  _startVideoCallScreen(String sessionId, int opponentId, BuildContext context) {

    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => VideoCallScreen(sessionId, opponentId)));
  }
}

mixin ErrorHandlingMixin<Page extends BaseStatefulWidget>
    on BaseStatefulWidgetState<Page> {
  HomeBloc bloc;

  @override
  void initState() {
    super.initState();

    this.bloc = HomeBloc();
    this
        .bloc
        .errorStream
        .listen((error) => showErrorSnackbar(error, scaffoldKey.currentState));
  }

  void showErrorSnackbar(String event, ScaffoldState context) {
    if (event != null) {
      context.removeCurrentSnackBar();
      context.showSnackBar(new SnackBar(
        content: new Text(event),
      ));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    this.bloc._errorStream.close();
    super.dispose();
  }
}

abstract class ErrorBloc {
  /// relays error information
  final _errorStream = StreamController<String>();

  Sink<String> get errorSink => _errorStream.sink;

  Stream<String> get errorStream => _errorStream.stream;
}

class HomeBloc extends ErrorBloc {}
