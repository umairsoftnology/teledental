import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Helper {

  static String validateEmail(String value) {

    value = value.trim();
    if (value.length < 7)
      return 'Invalid Id';
    else
      return null;

    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  static String validatePassword(String value) {

    value = value.trim();
    if (value.length < 3)
      return 'Invalid password';
    else
      return null;
  }

  static String validatePhoneNumber(String val) {

    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (val.length == 0) {
      return 'Please enter mobile number';
    }
    else if (!regExp.hasMatch(val)) {
      return 'Please enter valid mobile number \nexample (+/0 9 2 3452443958)';
    }
    return null;
  }

  static BoxDecoration boxDecor() {
    return BoxDecoration(
        color: Colors.white,
        border: Border.all(),
        borderRadius: BorderRadius.all(Radius.circular(6)),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 10, offset: Offset(1, 3))
        ]);
  }
}