import 'package:json_annotation/json_annotation.dart';
part 'tempClass.g.dart';

@JsonSerializable(explicitToJson: true)
class TempClassParent {
  List<TempClass> data;
  String status;

  TempClassParent({this.data, this.status});
  factory TempClassParent.fromJson(Map<String, dynamic> json) => _$TempClassParentFromJson(json);
  Map<String, dynamic> toJson() => _$TempClassParentToJson(this);
}


@JsonSerializable(explicitToJson: true)
class TempClass {
  String id;
  String employee_name;
  String employee_salary;
  String employee_age;
  String profile_image;

  TempClass({this.id, this.employee_name, this.employee_salary, this.employee_age, this.profile_image});
  factory TempClass.fromJson(Map<String, dynamic> json) => _$TempClassFromJson(json);
  Map<String, dynamic> toJson() => _$TempClassToJson(this);
}