import 'dart:io';

import 'package:flutter/cupertino.dart';
//import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';

enum MessageType { text, image, gif }

class ChatModel {
  String userId;
  String messageId;
  String text;
  String imageUrl;
  String GIFImageUrl;
  MessageType messageType;

//  ChatModel(
//      {@required this.userId,
//      @required this.messageId,
//      @required messageType,
//      this.text,
//      this.imageUrl,
//      this.GIFImageUrl});

  ChatModel.text(
      {@required this.userId,
        @required this.messageId,
        this.messageType = MessageType.text,
        @required this.text});

  ChatModel.gif(
      {@required this.userId,
        @required this.messageId,
        this.messageType = MessageType.gif,
        @required this.GIFImageUrl});

  ChatModel.image(
      {@required this.userId,
        @required this.messageId,
        this.messageType = MessageType.image,
        @required this.imageUrl});
}


class ChatModelManager extends ChangeNotifier {
  List<ChatModel> _messages = [];

  List<ChatModel> get messages {
    return _messages;
  }

  void onSendMessage(String id, String content, MessageType type,
      ScrollController scrollController,
      {File image, TextEditingController textEditingController}) {
    // type: 0 = text, 1 = image, 2 = sticker
    if (content.trim() != '') {
      ChatModel makeChatObject;

      switch (type) {
        case MessageType.image:
          makeChatObject = ChatModel.image(
              userId: id,
              messageId: DateTime.now().millisecondsSinceEpoch.toString(),
              imageUrl: content);
          break;
        case MessageType.text:
          makeChatObject = ChatModel.text(
              userId: id,
              messageId: DateTime.now().millisecondsSinceEpoch.toString(),
              text: content);
          textEditingController.clear();
          break;
        case MessageType.gif:
          makeChatObject = ChatModel.gif(
              userId: id,
              messageId: DateTime.now().millisecondsSinceEpoch.toString(),
              GIFImageUrl: content);
          break;
      }

      this._messages.add(makeChatObject);
//      this._messages.insert(0, makeChatObject);

      notifyListeners();

      Future.delayed(Duration(milliseconds: 100), () {
        scrollController.animateTo(scrollController.position.maxScrollExtent,
            curve: Curves.ease, duration: Duration(milliseconds: 300));
      });
    } else {
//      FlutterToast.showToast(msg: "Nothing to send");
//      Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }
}
