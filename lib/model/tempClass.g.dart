// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tempClass.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TempClassParent _$TempClassParentFromJson(Map<String, dynamic> json) {
  return TempClassParent(
    data: (json['data'] as List)
        ?.map((e) =>
            e == null ? null : TempClass.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$TempClassParentToJson(TempClassParent instance) =>
    <String, dynamic>{
      'data': instance.data?.map((e) => e?.toJson())?.toList(),
      'status': instance.status,
    };

TempClass _$TempClassFromJson(Map<String, dynamic> json) {
  return TempClass(
    id: json['id'] as String,
    employee_name: json['employee_name'] as String,
    employee_salary: json['employee_salary'] as String,
    employee_age: json['employee_age'] as String,
    profile_image: json['profile_image'] as String,
  );
}

Map<String, dynamic> _$TempClassToJson(TempClass instance) => <String, dynamic>{
      'id': instance.id,
      'employee_name': instance.employee_name,
      'employee_salary': instance.employee_salary,
      'employee_age': instance.employee_age,
      'profile_image': instance.profile_image,
    };
