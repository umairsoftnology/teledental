import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:quickblox_sdk/webrtc/constants.dart';
import 'package:quickblox_sdk/webrtc/rtc_video_view.dart';
import 'package:tele_medi/baseClasses/baseStatefulState.dart';
import 'package:tele_medi/services/callProvider.dart';

class TestingCall extends BaseStatefulWidget {

  final String _sessionId; // = "5d4175afa0eb4715cae5b63f";
  final int _opponentId; // = 3928;
  static final routeName = "videoCall";

  TestingCall([@required this._sessionId, @required this._opponentId]);



  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TestingCallState();
  }
}

class _TestingCallState extends BaseStatefulWidgetState<TestingCall>
    with BasicPageMixin {
  RTCVideoViewController _localVideoController;
  RTCVideoViewController _remoteVideoController;

  @override
  void initState() {
    super.initState();
    print("_VideoCallScreenState initState called");



    this._videoTrackReceived();
  }


  _videoTrackReceived() async {

    try {
      // QB.webrtc.subscribeRTCEvent(eventName, eventMethod)
      // await QB.webrtc.subscribeRTCEventTypes(QBRTCEventTypes.CALL, (data) {
      await QB.webrtc.subscribeRTCEvent(QBRTCEventTypes.RECEIVED_VIDEO_TRACK, (data) {
        print("video track received : $data");

        Future.delayed(Duration(seconds: 1)).then((value) {
          this._remoteVideoController.play(widget._sessionId, widget._opponentId);
        });
      });
    } on PlatformException catch (e) {
      print("QuickBlox new event exception : ${e.toString()}");
    }
  }

  @override
  void dispose() {
    this._localVideoController.release();
    this._remoteVideoController.release();
    print("_VideoCallScreenStated dispose called");
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("didChangeDependencies");
  }

  @override
  String screenName() {
    return "On Call";
  }

  @override
  Widget body(context) {
    print("body");
    return Column(children: <Widget>[
      Container(
        margin: new EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.width - 40,
        child: RTCVideoView(
          onVideoViewCreated: this._onLocalVideoViewCreated,
        ),
        decoration: new BoxDecoration(color: Colors.black54),
      ),
      Container(
        margin: new EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.width - 40,
        child: RTCVideoView(
          onVideoViewCreated: this._onRemoteVideoViewCreated,
        ),
        decoration: new BoxDecoration(color: Colors.black54),
      )
    ]);
  }

  void _onLocalVideoViewCreated(RTCVideoViewController controller) {

    this._localVideoController = controller;
    this._localVideoController.play(widget._sessionId, VideoCallManager().myQBLoginResult.qbUser.id);
  }

  void _onRemoteVideoViewCreated(RTCVideoViewController controller) {

    this._remoteVideoController = controller;
  }


  Future<void> play() async {
    this._localVideoController.play(widget._sessionId, VideoCallManager().myQBLoginResult.qbUser.id);
    this._remoteVideoController.play(widget._sessionId, widget._opponentId);
  }
}
