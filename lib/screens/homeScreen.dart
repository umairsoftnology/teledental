import 'package:tele_medi/model/tempClass.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tele_medi/baseClasses/baseStateLessState.dart';
import 'package:tele_medi/screens/settingsScreen.dart';

import 'makeCallScreen.dart';

enum HomeScreenMenu {
  bookAnAppointment,
  findADoctor,
  visitHistory,
  talkToAgent,
  appointmentProvider,
  videoCall,
  settings,
  tips,
  marketingAds
}

class HomeScreen extends BaseStatelessWidget {
  static const routeName = "homeScreen";
  List<TempClass> _array = [];

  /*
  @override
  Widget body() {
    return ListView.builder(
        itemCount: 100,
        itemBuilder: (ctx, index) {
          return Card(
              child: ListTile(
                  title: Text("$index"),
                  subtitle: Text("Age: $index")));
        });
  }
  */

  @override
  Widget body(context) {
    return Center(
      child: ListView(shrinkWrap: true, children: <Widget>[
        this._makeFlatButton("Book an Appointment", ctx,
            HomeScreenMenu.bookAnAppointment, context),
        this._makeFlatButton(
            "Find a Doctor", ctx, HomeScreenMenu.findADoctor, context),
        this._makeFlatButton(
            "Visit History", ctx, HomeScreenMenu.visitHistory, context),
        this._makeFlatButton(
            "Talk to Agent", ctx, HomeScreenMenu.talkToAgent, context),
        this._makeFlatButton("Appointment Provider", ctx,
            HomeScreenMenu.appointmentProvider, context),
        this._makeFlatButton(
            "Video Call", ctx, HomeScreenMenu.videoCall, context),
        this._makeFlatButton("Settings", ctx, HomeScreenMenu.settings, context),
        this._makeFlatButton("Tips", ctx, HomeScreenMenu.tips, context),
        this._makeFlatButton(
            "Marketing Ads", ctx, HomeScreenMenu.marketingAds, context),
      ]),
    );
  }

  Widget _makeFlatButton(String text, BuildContext ctx, HomeScreenMenu menu,
      BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 16),
        child: FlatButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          color: Theme.of(ctx).primaryColor,
          textColor: Colors.white,
          child: Text("$text"),
          onPressed: () => this._buttonTapped(menu, context),
        ));
  }

  _buttonTapped(HomeScreenMenu menu, BuildContext context) {
    switch (menu) {
      case HomeScreenMenu.bookAnAppointment:
        print(HomeScreenMenu.bookAnAppointment);
        break;
      case HomeScreenMenu.findADoctor:
        print(HomeScreenMenu.findADoctor);
        break;
      case HomeScreenMenu.visitHistory:
        print(HomeScreenMenu.visitHistory);
        break;
      case HomeScreenMenu.talkToAgent:
        print(HomeScreenMenu.talkToAgent);
        break;
      case HomeScreenMenu.appointmentProvider:
        print(HomeScreenMenu.appointmentProvider);
        break;
      case HomeScreenMenu.videoCall:
        Navigator.pushNamed(context, MakeCallScreen.routeName);
        break;
      case HomeScreenMenu.settings:
        Navigator.pushNamed(context, SettingsScreen.routeName);
        break;
      case HomeScreenMenu.tips:
        print(HomeScreenMenu.tips);
        break;
      case HomeScreenMenu.marketingAds:
        print(HomeScreenMenu.marketingAds);
        break;
    }
  }
}
