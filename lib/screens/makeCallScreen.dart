import 'package:tele_medi/baseClasses/baseStatefulState.dart';
import 'package:tele_medi/services/callProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MakeCallScreen extends BaseStatefulWidget {
  static final routeName = "MakeCallScreen";

  @override
  _MakeCallScreenState createState() => _MakeCallScreenState();
}

class _MakeCallScreenState extends BaseStatefulWidgetState<MakeCallScreen>
    with VideoCallMixin {
  final _textController = TextEditingController();

  @override
  String screenName() {
    return "Make Call";
  }

  @override
  void dispose() {
    this._textController.dispose();
    print("_MakeCallScreenState dispose called");
    super.dispose();
  }

  @override
  Widget body() {
    return Center(
        child: Container(
      margin: EdgeInsets.all(16),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
                keyboardType: TextInputType.number,
                controller: this._textController,
                decoration: InputDecoration(labelText: "Oponent Id")),
            SizedBox(
              width: double.infinity,
              child: FlatButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  onPressed: () {
                    VideoCallManager().makeCall(
                        int.parse(this._textController.text), context);
                  },
                  child: Text("Call")),
            )
          ]),
    ));
  }
}
