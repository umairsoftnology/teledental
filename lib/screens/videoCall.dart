import 'dart:async';
import 'dart:ffi';

import 'package:tele_medi/services/callProvider.dart';
import 'package:tele_medi/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:quickblox_sdk/webrtc/constants.dart';
import 'package:quickblox_sdk/webrtc/rtc_video_view.dart';

import '../baseClasses/baseStatefulState.dart';

/*
class VideoCallScreen extends StatefulWidget {
  final String sessionId; // = "5d4175afa0eb4715cae5b63f";
  final int opponentId; // = 3928;

  static final routeName = "videoCall";

  VideoCallScreen(
      {Key key, @required this.sessionId, @required this.opponentId})
      : super(key: key);

  @override
  _VideoCallScreenState createState() => _VideoCallScreenState();
}

class _VideoCallScreenState extends State<VideoCallScreen> {
  RTCVideoViewController _localVideoController;
  RTCVideoViewController _remoteVideoController;

  @override
  void initState() {
    print("initState");
    super.initState();

    Navigator.maybePop(context).then((value) {
      print("may be Pop");
    });
//
//    Future.delayed(Duration(seconds: 5)).then((value) {
//      this._remoteVideoListner();
//    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  _remoteVideoListner() {

    if (this._remoteVideoController == null) {
      return;
    }

    try {
      QB.webrtc
          .subscribeRTCEventTypes(QBRTCEventTypes.RECEIVED_VIDEO_TRACK, (data) async {
        Map<String, Object> map_complete = Map<String, dynamic>.from(data);
        Map<String, Object> map_payload =
            Map<String, dynamic>.from(map_complete[payload]);

        print("map_payload : $map_payload");

        print("going to play");
        if ((map_payload[userId] == this.widget.opponentId) &&
            (this._remoteVideoController != null)) {
          print("playing remote one");
          this._remoteVideoController.play(this.widget.sessionId, this.widget.opponentId);
        }

//        if ((map_payload[userId] == VideoCallManager().myQBId) &&
//            (this._localVideoController != null)) {
//          print("playing local one");
//          await this._localVideoController.play(this.widget.sessionId, VideoCallManager().myQBId);
//        }
      });
    } on PlatformException catch (e) {
      print("subscribeRTCEventTypes exception : ${e.toString()}");
    }
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
      title: Text('Call'),
    );

    return Scaffold(
        appBar: appBar,
        body: Column(children: [
          Container(
            margin: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
            width: MediaQuery.of(context).size.width,
            height: (MediaQuery.of(context).size.height -
                    appBar.preferredSize.height -
                    100) /
                2,
            decoration: const BoxDecoration(color: Colors.black54),
            child: RTCVideoView(
              onVideoViewCreated: this._onLocalVideoViewCreated,
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
            width: MediaQuery.of(context).size.width,
            height: (MediaQuery.of(context).size.height -
                    appBar.preferredSize.height -
                    100) /
                2,
            decoration: const BoxDecoration(color: Colors.black54),
            child: RTCVideoView(
                onVideoViewCreated: this._onRemoteVideoViewCreated),
          )
        ]));
  }

  Future<void> _onLocalVideoViewCreated(RTCVideoViewController controller) async {
    this._localVideoController = controller;
    Future.delayed(Duration(seconds: 1)).then((value) {
      this._localVideoController.play(this.widget.sessionId, VideoCallManager().myQBLoginResult.qbUser.id);
    });

//    await this._localVideoController.play(this.widget.sessionId, VideoCallManager().myQBId);
  }

  void _onRemoteVideoViewCreated(RTCVideoViewController controller) {
    this._remoteVideoController = controller;
    this._remoteVideoListner();
  }
}


*/

class VideoCallScreen extends BaseStatefulWidget {
  final String _sessionId; // = "5d4175afa0eb4715cae5b63f";
  final int _opponentId; // = 3928;
  static final routeName = "videoCall";

  VideoCallScreen([@required this._sessionId, @required this._opponentId]);

  @override
  State<StatefulWidget> createState() {
    return _VideoCallScreenState();
  }
}

class _VideoCallScreenState extends BaseStatefulWidgetState<VideoCallScreen>
    with BasicPageMixin {
  var _isMyScreenLarge = false;

  RTCVideoViewController _localVideoController;
  RTCVideoViewController _remoteVideoController;
  bool _isMute = false;

  @override
  void initState() {
    super.initState();
    print("_VideoCallScreenState initState called");
    this._videoTrackReceived();
  }

  @override
  void dispose() {
    this._localVideoController.release();
    this._remoteVideoController.release();
    print("_VideoCallScreenStated dispose called");
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("didChangeDependencies");
  }

  @override
  String screenName() {
    return "On Call";
  }

  @override
  Widget body(context) {
    print("body");
    return Stack(children: this._returnViews(context));
  }

  _videoTrackReceived() async {
    try {
      await QB.webrtc.subscribeRTCEvent(QBRTCEventTypes.RECEIVED_VIDEO_TRACK,
          (data) {
        print("video track received : $data");

        Future.delayed(Duration(seconds: 1)).then((value) {
          this
              ._remoteVideoController
              .play(widget._sessionId, widget._opponentId);
        });
      });
    } on PlatformException catch (e) {
      print("QuickBlox new event exception : ${e.toString()}");
    }
  }

  _mute() async {
    this._isMute != this._isMute;
    try {
      await QB.webrtc.enableAudio(widget._sessionId,
          enable: this._isMute,
          userId: VideoCallManager().myQBLoginResult.qbUser.id.toDouble());
      this.setState(() {});
    } on PlatformException catch (e) {
      // Some error occured, look at the exception message for more details
      print("not able to mute $e");
    }
  }

  _changeView() {
    print("change view");
    setState(() {
      this._isMyScreenLarge = !this._isMyScreenLarge;
    });
  }

  List<Widget> _returnViews(BuildContext context) {
    List<Widget> children = [];

    if (this._isMyScreenLarge == true) {
      children.add(_ScreenView(
          color: Colors.transparent,
          key: Key("me"),
          isScreenLarge: true,
          child: RTCVideoView(
            onVideoViewCreated: this._onLocalVideoViewCreated,
          ),
          callBack: this._changeView));
      children.add(_ScreenView(
          key: Key("friend"),
          isScreenLarge: false,
          color: Colors.transparent,
          child:
              RTCVideoView(onVideoViewCreated: this._onRemoteVideoViewCreated),
          callBack: this._changeView));
    } else {
      children.add(_ScreenView(
          key: Key("friend"),
          isScreenLarge: true,
          color: Colors.transparent,
          child:
              RTCVideoView(onVideoViewCreated: this._onRemoteVideoViewCreated),
          callBack: this._changeView));
      children.add(_ScreenView(
          key: Key("me"),
          isScreenLarge: false,
          color: Colors.transparent,
          child: RTCVideoView(
            onVideoViewCreated: this._onLocalVideoViewCreated,
          ),
          callBack: this._changeView));
    }

    children.add(Align(
        alignment: Alignment.bottomCenter,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          FlatButton(
              child: Icon(Icons.call_end),
              onPressed: () {
                VideoCallManager()
                    .endCall(widget._sessionId)
                    .then((value) => Navigator.pop(context));
              }),
          FlatButton(
              child: this._isMute
                  ? Icon(Icons.volume_mute)
                  : Icon(Icons.voice_chat),
              onPressed: this._mute)
        ])));

//    children.add(_ScreenView(
//        isScreenLarge: this._isMyScreenLarge, color: Colors.red,callBack: this._changeView));
//    children.add(_ScreenView(
//        isScreenLarge: !this._isMyScreenLarge, color: Colors.white, callBack: this._changeView));

    return children;
  }

  Future<void> _onLocalVideoViewCreated(
      RTCVideoViewController controller) async {
    this._localVideoController = controller;
    this._localVideoController.play(
        this.widget._sessionId, VideoCallManager().myQBLoginResult.qbUser.id);
  }

  void _onRemoteVideoViewCreated(RTCVideoViewController controller) {
    this._remoteVideoController = controller;
  }
}

class _ScreenView extends StatelessWidget {
  const _ScreenView(
      {Key key,
      @required bool isScreenLarge,
      @required color,
      @required child,
      @required callBack})
      : _isScreenLarge = isScreenLarge,
        _colr = color,
        _child = child,
        _callBack = callBack,
        super(key: key);

  final int _animationDuration = 300;
  final _isScreenLarge;
  final Function() _callBack;
  final Color _colr;
  final Widget _child;

  @override
  Widget build(BuildContext context) {
    var radius = 0.0;
    var padding = 0.0;
    var width = 0.0;
    var height = 0.0;

    if (this._isScreenLarge) {
      width = MediaQuery.of(context).size.width;
      height = MediaQuery.of(context).size.height;
      padding = 0.0;
      radius = 0.0;
    } else {
      width = 70.0;
      height = 100.0;
      padding = 20.0;
      radius = 8.0;
    }

    print("key : $key");
    print("width : $width");
    print("height : $height");
    print("colr : ${this._colr}");
    print("radius : $radius");
    print("padding : $padding");
    print("_isScreenLarge : ${this._isScreenLarge}");

    return AnimatedPositioned(
        duration: this._isScreenLarge
            ? Duration.zero
            : Duration(milliseconds: this._animationDuration),
        bottom: padding,
        right: padding,
        width: width,
        height: height,
        child: GestureDetector(
          onTap: this._callBack,
          child: Container(
            clipBehavior: Clip.hardEdge,
            child: this._child,
            decoration: BoxDecoration(
                color: this._colr,
                borderRadius: BorderRadius.all(Radius.circular(radius))),
            alignment: Alignment.bottomCenter,
          ),
        ));
  }
}
