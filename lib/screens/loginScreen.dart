import 'dart:async';

import 'package:tele_medi/model/album.dart';
import 'package:tele_medi/baseClasses/baseStatefulState.dart';
import 'package:tele_medi/baseClasses/tabbarController.dart';
import 'package:tele_medi/screens/signUpScreen.dart';
import 'package:tele_medi/services/callProvider.dart';
import 'package:tele_medi/utils/helper.dart';
import 'package:tele_medi/webAPI/webAPIs.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'homeScreen.dart';
import 'makeCallScreen.dart';

class LoginScreen extends BaseStatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }

}

class LoginScreenState extends BaseStatefulWidgetState<LoginScreen>
    with BasicPageMixin, ErrorHandlingMixin {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  Future<Album> _futureAlbum;

  String _email;
  String _password;

  @override
  String screenName() {
    return "Login";
  }

  @override
  void initState() {
    super.initState();
    WebAPIs.fetchAlbum().then((value) {
     print(value);
    });
  }

//
  @override
  Widget body(context) {
    return Center(
        child: Container(
      margin: EdgeInsets.all(16),
      padding: EdgeInsets.all(16),
      decoration: Helper.boxDecor(),
      child: Form(
          autovalidate: this._autoValidate,
          key: _formKey,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            TextFormField(
                keyboardType: TextInputType.emailAddress,
                validator: Helper.validateEmail,
                onSaved: (String val) {
                  this._email = val;
                },
                decoration: InputDecoration(labelText: "Enter Id")),
            TextFormField(
                obscureText: true,
                keyboardType: TextInputType.text,
                validator: Helper.validatePassword,
                onSaved: (String val) {
                  this._password = val;
                },
                decoration: InputDecoration(labelText: "Enter password")),
            SizedBox(
                width: double.infinity,
                child: FlatButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  child: Text("Login"),
                  onPressed: () {
                    this._validateInputs(context);
                  },
                )),
            Align(
                alignment: Alignment(1, 0),
                child: FlatButton(
                  onPressed: () {},
                  child: Text("Forgotten your password?"),
                  textColor: Colors.blue,
                )),
            Align(
                alignment: Alignment(1, 0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(SignUpScreen.routeName);
                  },
                  child: Text("Signup"),
                  textColor: Colors.blue,
                ))
          ])),
    ));
  }

  @override
  dispose() {
    super.dispose();
  }

  Future<void> _handleCameraAndMic() async {
    await Permission.camera.request();
    await Permission.microphone.request();
  }

  void _validateInputs(BuildContext ctx) {
    _handleCameraAndMic();
    /*
    Navigator.of(ctx).pushNamed(ChatWindow.routeName);
    return;


    WebAPIs.fetchTempClassData().then((value) async {
            Navigator.of(ctx).pushNamed(HomeScreen.routeName, arguments: value.data);
    });
    */

    FocusScope.of(ctx).requestFocus(FocusNode());
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      VideoCallManager().QBLogin(this._email, this._password, ctx,
          (isSucceed, error) {
        print(isSucceed);
        if (isSucceed == false) {
          this.bloc.errorSink.add(error);
        } else {
          Navigator.of(ctx).push(CupertinoPageRoute(
              fullscreenDialog: true, builder: (context) => TabScreen()));
        }
      });
//      VideoCallManager().QBLogin(this._email, this._password, context);
//      VideoCallManager().quickBloxLogin(ctx, this._email, this._password, int.parse(this._opponentId));
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
