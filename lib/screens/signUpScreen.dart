import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:tele_medi/baseClasses/baseStateLessState.dart';

class SignUpScreen extends BaseStatelessWidget {
  static const routeName = "SignUpScreen";

  @override
  Widget body(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(16),
        child: Column(children: <Widget>[
          TextFormField(decoration: InputDecoration(labelText: "First Name")),
          TextFormField(decoration: InputDecoration(labelText: "Last Name")),
          TextFormField(decoration: InputDecoration(labelText: "Address")),
          TextFormField(decoration: InputDecoration(labelText: "City")),
          TextFormField(decoration: InputDecoration(labelText: "Country")),
          TextFormField(decoration: InputDecoration(labelText: "Contact#"))
        ]));
  }

  @override
  Widget appBar() {
    return AppBar(title: Text("SignUp"));
  }
}
