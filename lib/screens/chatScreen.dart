/*

import 'dart:async';
import 'dart:io';

import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:doctorpatient/model/chat.dart';
import 'package:doctorpatient/widgets/fullImage.dart';
import 'package:doctorpatient/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:doctorpatient/utils/constants.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'call.dart';

class Chat extends StatelessWidget {
  final String peerId;
  final String peerAvatar;
  static final routeName = "chatScreen";

  Chat({Key key, @required this.peerId, @required this.peerAvatar})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed(CallPage.routeName,
                      arguments: ["abc_channel", ClientRole.Audience]);
                },
                child: Icon(
                  Icons.call,
                  size: 26.0,
                ),
              ))
        ],
        title: Text(
          'CHAT',
          style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: ChatScreen(
        peerId: peerId,
        peerAvatar: peerAvatar,
      ),
    );
  }
}

class ChatScreen extends StatefulWidget {
  final String peerId;
  final String peerAvatar;

  ChatScreen({Key key, @required this.peerId, @required this.peerAvatar})
      : super(key: key);

  @override
  State createState() =>
      ChatScreenState(peerId: peerId, peerAvatar: peerAvatar);
}

class ChatScreenState extends State<ChatScreen> {
  ChatScreenState({Key key, @required this.peerId, @required this.peerAvatar});

  String peerId;
  String peerAvatar;
  String id = "1";
  bool isShowSticker;

  String groupChatId = "adsf";

//  SharedPreferences prefs;

  ChatModelManager chatModel;

  File imageFile;
  bool isLoading;
  String imageUrl;

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController listScrollController = ScrollController();
  final FocusNode focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    this.focusNode.addListener(onFocusChange);

//    groupChatId = '';

    this.isLoading = false;
    this.isShowSticker = false;
    this.imageUrl = '';

    readLocal();
  }

  void onFocusChange() {
    if (this.focusNode.hasFocus) {
      // Hide sticker when keyboard appear
      setState(() {
        this.isShowSticker = false;
      });
    }
  }

  readLocal() async {
//    prefs = await SharedPreferences.getInstance();
//    id = prefs.getString('id') ?? '';
//    if (id.hashCode <= peerId.hashCode) {
//      groupChatId = '$id-$peerId';
//    } else {
//      groupChatId = '$peerId-$id';
//    }

//    Firestore.instance
//        .collection('users')
//        .document(id)
//        .updateData({'chattingWith': peerId});

    setState(() {});
  }

  @override
  void dispose() {
    this.textEditingController.dispose();
    this.listScrollController.dispose();
    this.focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("build called");
    this.chatModel = Provider.of<ChatModelManager>(context);

    // TODO: implement build
    return WillPopScope(
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              // List of messages
              this.buildListMessage(),

              // Sticker
              (this.isShowSticker ? this.buildSticker() : Container()),

              // Input content
              this.buildInput(),
            ],
          ),

          // Loading
          this.buildLoading()
        ],
      ),
      onWillPop: this.onBackPress,
    );
  }

  Future<bool> onBackPress() {
    if (this.isShowSticker) {
      setState(() {
        this.isShowSticker = false;
      });
    } else {
//      Firestore.instance
//          .collection('users')
//          .document(id)
//          .updateData({'chattingWith': null});
      Navigator.pop(context);
    }

    return Future.value(false);
  }

  Widget buildSticker() {
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi1",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi1.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi2",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi2.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi3",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi3.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi4",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi4.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi5",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi5.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi6",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi6.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi7",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi7.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi8",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi8.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => this.chatModel.onSendMessage(this.id, "mimi9",
                    MessageType.gif, this.listScrollController),
                child: Image.asset(
                  'images/mimi9.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: greyColor2, width: 0.5)),
          color: Colors.white),
      padding: EdgeInsets.all(5.0),
      height: 180.0,
    );
  }

  Widget buildLoading() {
    return Positioned(
      child: this.isLoading ? const Loading() : Container(),
    );
  }

  Widget buildListMessage() {
    return Flexible(
        child: groupChatId == ''
            ? Center(
                child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).primaryColor)))
            : this.listV);
  }

  Widget get listV {
    return GestureDetector(
      onTap: ()=> FocusScope.of(context).unfocus(),
      child: ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemBuilder: (context, index) =>
            buildItem(index, this.chatModel.messages[index]),
        itemCount: this.chatModel.messages.length,
        reverse: true,
        controller: this.listScrollController,
      ),
    );
  }

  Widget buildItem(int index, ChatModel obj) {
    if (obj.userId == id) {
      // Right (my message)
      return Row(
        children: <Widget>[
          obj.messageType == MessageType.text
              // Text
              ? Container(
                  child: Text(
                    obj.text,
                    style: TextStyle(color: Theme.of(context).accentColor),
//                    textAlign: TextAlign.end,
                  ),
                  padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                  width: 200.0,
                  decoration: BoxDecoration(
                      color: greyColor2,
                      borderRadius: BorderRadius.circular(8.0)),
                  margin: EdgeInsets.only(
                      bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                      right: 10.0),
                )
              : obj.messageType == MessageType.image
                  // Image
                  ? Container(
                      child: FlatButton(
                        child: Material(
                          child: Image.file(
                            File(obj.imageUrl),
                            width: 200.0,
                            height: 200.0,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          clipBehavior: Clip.hardEdge,
                        ),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      FullPhoto(url: obj.imageUrl)));
                        },
                        padding: EdgeInsets.all(0),
                      ),
                      margin: EdgeInsets.only(
                          bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                          right: 10.0),
                    )
                  // Sticker
                  : Container(
                      child: Image.asset(
                        'images/${obj.GIFImageUrl}.gif',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.cover,
                      ),
                      margin: EdgeInsets.only(
                          bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                          right: 10.0),
                    ),
        ],
        mainAxisAlignment: MainAxisAlignment.end,
      );
    } else {
      // Left (peer message)
      return Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                isLastMessageLeft(index)
                    ? Material(
                        child: CachedNetworkImage(
                          placeholder: (context, url) => Container(
                            child: CircularProgressIndicator(
                              strokeWidth: 1.0,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor),
                            ),
                            width: 35.0,
                            height: 35.0,
                            padding: EdgeInsets.all(10.0),
                          ),
                          imageUrl: peerAvatar,
                          width: 35.0,
                          height: 35.0,
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(18.0),
                        ),
                        clipBehavior: Clip.hardEdge,
                      )
                    : Container(width: 35.0),
                obj.messageType == MessageType.text
                    ? Container(
                        child: Text(
                          obj.text,
                          style: TextStyle(color: Colors.white),
                        ),
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                        width: 200.0,
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(8.0)),
                        margin: EdgeInsets.only(left: 10.0),
                      )
                    : obj.messageType == MessageType.image
                        ? Container(
                            child: FlatButton(
                              child: Material(
                                child: CachedNetworkImage(
                                  placeholder: (context, url) => Container(
                                    child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Theme.of(context).primaryColor),
                                    ),
                                    width: 200.0,
                                    height: 200.0,
                                    padding: EdgeInsets.all(70.0),
                                    decoration: BoxDecoration(
                                      color: greyColor2,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(8.0),
                                      ),
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Material(
                                    child: Image.asset(
                                      'images/img_not_available.jpeg',
                                      width: 200.0,
                                      height: 200.0,
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8.0),
                                    ),
                                    clipBehavior: Clip.hardEdge,
                                  ),
                                  imageUrl: obj.imageUrl,
                                  width: 200.0,
                                  height: 200.0,
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8.0)),
                                clipBehavior: Clip.hardEdge,
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            FullPhoto(url: obj.imageUrl)));
                              },
                              padding: EdgeInsets.all(0),
                            ),
                            margin: EdgeInsets.only(left: 10.0),
                          )
                        : Container(
                            child: Image.asset(
                              'images/${obj.GIFImageUrl}.gif',
                              width: 100.0,
                              height: 100.0,
                              fit: BoxFit.cover,
                            ),
                            margin: EdgeInsets.only(
                                bottom: isLastMessageRight(index) ? 20.0 : 10.0,
                                right: 10.0),
                          ),
              ],
            ),

            // Time
            isLastMessageLeft(index)
                ? Container(
                    child: Text(
                      DateFormat('dd MMM kk:mm').format(
                          DateTime.fromMillisecondsSinceEpoch(
                              int.parse(obj.messageId))),
                      style: TextStyle(
                          color: greyColor,
                          fontSize: 12.0,
                          fontStyle: FontStyle.italic),
                    ),
                    margin: EdgeInsets.only(left: 50.0, top: 5.0, bottom: 5.0),
                  )
                : Container()
          ],
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
        margin: EdgeInsets.only(bottom: 10.0),
      );
    }
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 &&
            this.chatModel.messages != null &&
            this.chatModel.messages[index - 1].userId == id) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            this.chatModel.messages != null &&
            this.chatModel.messages[index - 1].userId != id) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.image),
                onPressed: this.getImage,
                color: Theme.of(context).primaryColor,
              ),
            ),
            color: Colors.white,
          ),
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 1.0),
              child: IconButton(
                icon: Icon(Icons.face),
                onPressed: this.getSticker,
                color: Theme.of(context).primaryColor,
              ),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 15.0),
                controller: this.textEditingController,
                decoration: InputDecoration.collapsed(
                  hintText: 'Type your message...',
                  hintStyle: TextStyle(color: greyColor),
                ),
                focusNode: this.focusNode,
              ),
            ),
          ),

          // Button send message
          Material(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: () => this.chatModel.onSendMessage(
                    this.id,
                    this.textEditingController.text,
                    MessageType.text,
                    this.listScrollController,
                    textEditingController: this.textEditingController),
                color: Theme.of(context).primaryColor,
              ),
            ),
            color: Colors.white,
          ),
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: greyColor2, width: 0.5)),
          color: Colors.white),
    );
  }

  Future getImage() async {
//    imageFile = await ImagePicker().getImage(source: ImageSource.gallery);
//
//    if (imageFile != null) {
//      setState(() {
//        isLoading = true;
//      });
//      uploadFile();
//    }

    // using your method of getting an image
    final pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    final file = File(pickedFile.path);

// getting a directory path for saving
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    var firstPath = documentDirectory.path + "/images"; //%%%
    //You'll have to manually create subdirectories
    await Directory(firstPath).create(recursive: true); //%%%
    // Name the file, create the file, and save in byte form.
    var filePathAndName = documentDirectory.path +
        '/images/${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';

// copy the file to a new path
    final File newImage = await file.copy('$filePathAndName');

    this.chatModel.onSendMessage(
        this.id, filePathAndName, MessageType.image, this.listScrollController,
        image: newImage);
//    this.onSendMessage(filePathAndName, MessageType.image, newImage);
  }

  void getSticker() {
    // Hide keyboard when sticker appear
    this.focusNode.unfocus();
    setState(() {
      this.isShowSticker = !this.isShowSticker;
    });
  }

/*
  void onSendMessage(String content, MessageType type, [File image]) {
    // type: 0 = text, 1 = image, 2 = sticker
    if (content.trim() != '') {
      ChatModel makeChatObject;

      switch (type) {
        case MessageType.image:
          makeChatObject = ChatModel.image(
              userId: this.id,
              messageId: DateTime.now().millisecondsSinceEpoch.toString(),
              imageUrl: content);
          break;
        case MessageType.text:
          makeChatObject = ChatModel.text(
              userId: this.id,
              messageId: DateTime.now().millisecondsSinceEpoch.toString(),
              text: content);
          this.textEditingController.clear();
          break;
        case MessageType.gif:
          makeChatObject = ChatModel.gif(
              userId: this.id,
              messageId: DateTime.now().millisecondsSinceEpoch.toString(),
              GIFImageUrl: content);
          break;
      }



      Future.delayed(Duration.zero).then((value) {
//        this.listScrollController.animateTo((this.messages.length +
//            1/this.messages.length)*this.listScrollController.position.maxScrollExtent,duration: const
//        Duration(milliseconds: 300),curve: Curves.easeOut);

        this.listScrollController.animateTo(0.0,
            duration: Duration(milliseconds: 300), curve: Curves.easeOut);

//        this.listScrollController.animateTo(0.0,
//            duration: Duration(milliseconds: 300), curve: Curves.easeOut);
      });

      this.setState(() {
        this.messages.insert(0, makeChatObject);
      });
      /*
      Future.delayed(Duration.zero, () {

        this.listScrollController.animateTo(0.0,
            duration: Duration(milliseconds: 300), curve: Curves.easeOut);
//          _scrollController.animateTo((_messages.length +
//              1/_messages.length)*_scrollController.position.maxScrollExtent,duration: const
//          Duration(milliseconds: 300),curve: Curves.easeOut);
      });
      */
    } else {
      Fluttertoast.showToast(msg: 'Nothing to send');
    }
  }
*/
}

****/
