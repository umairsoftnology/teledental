import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:tele_medi/baseClasses/baseStatefulState.dart';

class StackView extends BaseStatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StackViewState();
  }
}

class StackViewState extends BaseStatefulWidgetState<StackView> with BasicPageMixin {
  var _isMyScreenLarge = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("initState");
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    print("didChangeDependencies");
  }

  @override
  Widget body(context) {
    // TODO: implement body
    print("body");
    return Stack(children: this._returnViews());
  }

  @override
  String screenName() {
    // TODO: implement screenName
    return "Stack View";
  }

  _changeView() {
    setState(() {
      this._isMyScreenLarge = !this._isMyScreenLarge;
    });
  }

  List<Widget> _returnViews() {
    List<Widget> children = [];

    if (this._isMyScreenLarge == true) {
      children.add(_ScreenView(
          key: Key("me"),
          isScreenLarge: true,
          color: Colors.red,
          callBack: this._changeView));
      children.add(_ScreenView(
          key: Key("friend"),
          isScreenLarge: false,
          color: Colors.white,
          callBack: this._changeView));
    } else {
      children.add(_ScreenView(
          key: Key("friend"),
          isScreenLarge: true,
          color: Colors.white,
          callBack: this._changeView));
      children.add(_ScreenView(
          key: Key("me"),
          isScreenLarge: false,
          color: Colors.red,
          callBack: this._changeView));
    }

//    children.add(_ScreenView(
//        isScreenLarge: this._isMyScreenLarge, color: Colors.red,callBack: this._changeView));
//    children.add(_ScreenView(
//        isScreenLarge: !this._isMyScreenLarge, color: Colors.white, callBack: this._changeView));

    return children;
  }
}

class _ScreenView extends StatelessWidget {
  const _ScreenView(
      {Key key,
      @required bool isScreenLarge,
      @required color,
      @required callBack})
      : _isScreenLarge = isScreenLarge,
        _colr = color,
        _callBack = callBack,
        super(key: key);

  final int _animationDuration = 300;
  final _isScreenLarge;
  final Function() _callBack;
  final Color _colr;

  @override
  Widget build(BuildContext context) {
    var radius = 0.0;
    var padding = 0.0;
    var width = 0.0;
    var height = 0.0;

    if (this._isScreenLarge) {
      width = MediaQuery.of(context).size.width;
      height = MediaQuery.of(context).size.height;
      padding = 0.0;
      radius = 0.0;
    } else {
      width = 50.0;
      height = 100.0;
      padding = 20.0;
      radius = 8.0;
    }

    print("key : $key");
    print("width : $width");
    print("height : $height");
    print("colr : ${this._colr}");
    print("radius : $radius");
    print("padding : $padding");
    print("_isScreenLarge : ${this._isScreenLarge}");

    return AnimatedPositioned(
        duration: this._isScreenLarge
            ? Duration.zero
            : Duration(milliseconds: this._animationDuration),
        bottom: padding,
        right: padding,
        width: width,
        height: height,
        child: GestureDetector(
          onTap: this._callBack,
          child: Container(
            decoration: BoxDecoration(
                color: this._colr,
                borderRadius: BorderRadius.all(Radius.circular(radius))),
            alignment: Alignment.bottomCenter,
          ),
        ));
  }
}
