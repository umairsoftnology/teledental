import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:tele_medi/baseClasses/baseStatefulState.dart';
import 'package:tele_medi/utils/helper.dart';

class SettingsScreen extends BaseStatefulWidget {
  static final routeName = "settingsScreen";

  @override
  State<StatefulWidget> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends BaseStatefulWidgetState with BasicPageMixin {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _password;
  String _phoneNumber;

  @override
  String screenName() {
    return "Settings";
  }

  @override
  Widget body(context) {
    return Container(
      margin: EdgeInsets.all(16),
      padding: EdgeInsets.all(16),
//          decoration: Helper.boxDecor(),
      child: Form(
          autovalidate: this._autoValidate,
          key: _formKey,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            TextFormField(
                keyboardType: TextInputType.emailAddress,
                validator: Helper.validateEmail,
                onSaved: (String val) {
                  this._email = val;
                },
                decoration: InputDecoration(labelText: "First Name")),
            TextFormField(
                keyboardType: TextInputType.text,
                validator: Helper.validatePassword,
                onSaved: (String val) {
                  this._password = val;
                },
                decoration: InputDecoration(labelText: "Last Name")),
            TextFormField(
                keyboardType: TextInputType.number,
                validator: Helper.validatePhoneNumber,
                onSaved: (String val) {
                  this._phoneNumber = val;
                },
                decoration: InputDecoration(
                    errorMaxLines: 2, labelText: "Update Phone Number")),
            SizedBox(
                width: double.infinity,
                child: FlatButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  child: Text("Save"),
                  onPressed: () {
                    this._validateInputs(context);
                  },
                ))
          ])),
    );
  }

  void _validateInputs(BuildContext ctx) {
    FocusScope.of(ctx).requestFocus(FocusNode());
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }
}
