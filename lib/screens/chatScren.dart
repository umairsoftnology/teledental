import 'dart:io';
import 'package:tele_medi/model/chat.dart';
import 'package:tele_medi/widgets/fullImage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class ChatWindow extends StatefulWidget {
  static final routeName = "chatScreen";

  @override
  _ChatWindowState createState() => _ChatWindowState();
}

class _ChatWindowState extends State<ChatWindow> {
  TextEditingController _textEditingController;
  ScrollController _scrollController;
  final _myId = "122";
  final FocusNode _focusNode = FocusNode();
  bool _isShowSticker;
  bool enableButton = false;

  @override
  void initState() {
    super.initState();

    this._isShowSticker = false;
    this._focusNode.addListener(this.onFocusChange);

    this._textEditingController = TextEditingController();
    this._scrollController = ScrollController();
  }

  @override
  void dispose() {
    this._textEditingController.dispose();
    this._scrollController.dispose();
    this._focusNode.dispose();
    super.dispose();
  }

  void onFocusChange() {
    if (this._focusNode.hasFocus) {
      // Hide sticker when keyboard appear
      setState(() {
        this._isShowSticker = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text("Chat Bubbles"),
      ),
      body: Column(
        children: <Widget>[
          ChatList(scrollController: this._scrollController),
          Divider(height: 2.0),
          (this._isShowSticker
              ? BuildStickers(
              scrollController: this._scrollController, userId: this._myId)
              : Container()),
          this.inputBar(context, this._scrollController)
        ],
      ),
    );
  }

  Row inputBar(BuildContext context, ScrollController scrollController) {
    ChatModelManager _chatModel =
    Provider.of<ChatModelManager>(context, listen: false);
    return Row(
      children: <Widget>[
        Material(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 1.0),
            child: IconButton(
              icon: Icon(Icons.image),
              onPressed: () {
                this.getImage(_chatModel);
              },
              color: Theme.of(context).primaryColor,
            ),
          ),
          color: Colors.white,
        ),
        Material(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 1.0),
            child: IconButton(
              icon: Icon(Icons.face),
              onPressed: this.getSticker,
              color: Theme.of(context).primaryColor,
            ),
          ),
          color: Colors.white,
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: TextField(
              focusNode: this._focusNode,
              onChanged: (text) {
                setState(() {
                  this.enableButton = text.isNotEmpty;
                });
              },
              decoration: InputDecoration.collapsed(
                hintText: "Type a message",
              ),
              controller: this._textEditingController,
            ),
          ),
        ),
        this.enableButton
            ? IconButton(
          color: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.send,
          ),
          disabledColor: Colors.grey,
          onPressed: () {
            _chatModel.onSendMessage(
                this._myId,
                this._textEditingController.text,
                MessageType.text,
                scrollController,
                textEditingController: this._textEditingController);
          },
        )
            : IconButton(
          color: Colors.blue,
          icon: Icon(
            Icons.send,
          ),
          disabledColor: Colors.grey,
          onPressed: null,
        )
      ],
    );
  }

  Future getImage(ChatModelManager chatModel) async {
    // using your method of getting an image
    final pickedFile =
    await ImagePicker().getImage(source: ImageSource.gallery);
    final file = File(pickedFile.path);

// getting a directory path for saving
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    var firstPath = documentDirectory.path + "/images"; //%%%
    //You'll have to manually create subdirectories
    await Directory(firstPath).create(recursive: true); //%%%
    // Name the file, create the file, and save in byte form.
    var filePathAndName = documentDirectory.path +
        '/images/${DateTime.now().millisecondsSinceEpoch.toString()}.jpg';

// copy the file to a new path
    final File newImage = await file.copy('$filePathAndName');

    chatModel.onSendMessage(
        this._myId, filePathAndName, MessageType.image, this._scrollController,
        image: newImage);
//    this.onSendMessage(filePathAndName, MessageType.image, newImage);
  }

  void getSticker() {
    // Hide keyboard when sticker appear
    this._focusNode.unfocus();
    setState(() {
      this._isShowSticker = !this._isShowSticker;
    });
  }
}

class BuildStickers extends StatelessWidget {
  final ScrollController scrollController;
  final String userId;

  const BuildStickers(
      {Key key, @required this.scrollController, @required this.userId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    ChatModelManager _chatModelManager =
    Provider.of<ChatModelManager>(context, listen: false);
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi1", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi1.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi2", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi2.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi3", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi3.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi4", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi4.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi5", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi5.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi6", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi6.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi7", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi7.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi8", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi8.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              ),
              FlatButton(
                onPressed: () => _chatModelManager.onSendMessage(this.userId,
                    "mimi9", MessageType.gif, this.scrollController),
                child: Image.asset(
                  'images/mimi9.gif',
                  width: 50.0,
                  height: 50.0,
                  fit: BoxFit.cover,
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      decoration: BoxDecoration(
          border: Border(top: BorderSide(color: Colors.grey[200], width: 0.5)),
          color: Colors.white),
      padding: EdgeInsets.all(5.0),
      height: 180.0,
    );
  }
}

class ChatList extends StatelessWidget {
  const ChatList({
    Key key,
    @required this.scrollController,
  }) : super(key: key);

  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    ChatModelManager _chatModel = Provider.of<ChatModelManager>(context);

    return Expanded(
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: ListView.builder(
          controller: scrollController,
          itemCount: _chatModel.messages.length,
          itemBuilder: (context, index) {
            bool reverse = false;
            if (index % 2 == 0) {
              reverse = true;
            }

            var triangle = CustomPaint(
              painter: Triangle(),
            );

            Widget stackWidget;

            if (reverse) {
              stackWidget = Stack(
                children: <Widget>[
                  MessageBody(message: _chatModel.messages[index]),
                  Positioned(right: 0, bottom: 0, child: triangle),
                ],
              );
            } else {
              stackWidget = Stack(
                children: <Widget>[
                  Positioned(left: 0, bottom: 0, child: triangle),
                  MessageBody(message: _chatModel.messages[index]),
                ],
              );
            }

            return this.makeRowWithDirection(reverse, stackWidget);
          },
        ),
      ),
    );
  }

  Row makeRowWithDirection(bool reverse, Widget stackWidget) {
    if (reverse) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: stackWidget,
          ),
          Avatar(),
        ],
      );
    } else {
      return Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Avatar(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: stackWidget,
          ),
        ],
      );
    }
  }
}

class MessageBody extends StatelessWidget {
  const MessageBody({
    Key key,
    @required ChatModel message,
  })  : _message = message,
        super(key: key);

  final ChatModel _message;

  @override
  Widget build(BuildContext context) {
    Widget widget;
    switch (this._message.messageType) {
      case MessageType.text:
        widget = this.textMessage(this._message.text, context);
        break;
      case MessageType.image:
        widget = this.imageMessage(context, this._message.imageUrl);
        break;
      case MessageType.gif:
        widget = this.gifMessage(this._message.GIFImageUrl);
        break;
    }

    return ConstrainedBox(
      constraints:
      BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.65),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.amber,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: widget,
        ),
      ),
    );
  }

  Image gifMessage(String gifURL) {
    return Image.asset(
      'images/${gifURL}.gif',
      width: 100.0,
      height: 100.0,
      fit: BoxFit.cover,
    );
  }

  FlatButton imageMessage(BuildContext context, String imageUrl) {
    return FlatButton(
      child: Material(
        child: Image.file(
          File(imageUrl),
          width: 200.0,
          height: 200.0,
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(8.0),
        ),
        clipBehavior: Clip.hardEdge,
      ),
      onPressed: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => FullPhoto(url: imageUrl)));
      },
      padding: EdgeInsets.all(0),
    );
  }

  Widget textMessage(String text, BuildContext context) {
    return Text(text);
    return ConstrainedBox(
      constraints:
      BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.65),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.amber,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(text),
        ),
      ),
    );
  }
}

class Avatar extends StatelessWidget {
  const Avatar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, bottom: 8.0, right: 8.0),
      child: CircleAvatar(
        child: Text("A"),
      ),
    );
  }
}

class Triangle extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()..color = Colors.amber;

    var path = Path();
    path.lineTo(10, 0);
    path.lineTo(0, -10);
    path.lineTo(-10, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
