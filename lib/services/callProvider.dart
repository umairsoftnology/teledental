import 'dart:async';

import 'package:tele_medi/screens/videoCall.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quickblox_sdk/auth/module.dart';
import 'package:quickblox_sdk/mappers/qb_rtc_session_mapper.dart';
import 'package:quickblox_sdk/models/qb_rtc_session.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'package:quickblox_sdk/webrtc/constants.dart';

class VideoCallManager {
  static final VideoCallManager _singleton = VideoCallManager._internal();

  QBLoginResult myQBLoginResult;
  Function(bool, String) callBack;
  final StreamController<dynamic> _controller = StreamController<dynamic>();

  factory VideoCallManager() {
    return _singleton;
  }

  VideoCallManager._internal();

  /*
  Future<QBUser> quickBloxLogin(BuildContext context, String myId,
      String password, int opponentId) async {
    QBUser user;

    try {
      QBLoginResult result = await QB.auth.login(myId, password);
      user = result.qbUser;

      print(user.id);
      print(user.blobId);
      print(user.email);

      await QB.chat.connect(user.id, password);

      await QB.webrtc.init();

      QBRTCSession session_ =
          await QB.webrtc.call([113576768], QBRTCSessionTypes.VIDEO);



      this._startVideoCallScreen(session_.id, user.id, opponentId, context);
    } on PlatformException catch (e) {
      print("QuickBlox exception : ${e.toString()}");
      return null;
    }
  }
  */


  Stream<dynamic> get stream => this._controller.stream;

  QBLogin(String myId, String password, BuildContext context, callBack) async {

    var isConnected = await QB.chat.isConnected();
    if (isConnected) {
      callBack(true, null);
      return;
    }

    this._QbLogin(myId, password, context, callBack);
  }

  _QbLogin(String myId, String password, BuildContext context, callBack) async {

    try {
      QBLoginResult result = await QB.auth.login(myId, password);
      this.myQBLoginResult = result;
      this._QBConnect(password, context, callBack);
    } on PlatformException catch (e) {

      print("QBLogin exception : ${e.message}");
      if (e.code.contains("401")) {
        callBack(false, "Invalid User Id/Password");
      }
      else {
        callBack(false, e.toString());
      }
    }
  }

  _QBConnect(String password, BuildContext context, dynamic callBack) async {
    try {
      await QB.chat.connect(this.myQBLoginResult.qbUser.id, password);
      this._QBWebRTCInit(context, callBack);
    } on PlatformException catch (e) {
      print("QBConnect exception : ${e.toString()}");
      callBack(false, e.toString());
    }
  }

  _QBWebRTCInit(BuildContext context, dynamic callBack) async {
    try {
      await QB.webrtc.init();
      this._monitorCalls(context, this.myQBLoginResult.qbUser.id);
      callBack(true, null);
    } on PlatformException catch (e) {
      print("QBWebRTCInit exception : ${e.toString()}");
      callBack(false, e.toString());
    }
  }

  _startVideoCallScreen(
      String sessionId, int opponentId, BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => VideoCallScreen(sessionId, opponentId)));
  }

  makeCall(int opponenetId, BuildContext context) async {
    QBRTCSession session =
        await QB.webrtc.call([opponenetId], QBRTCSessionTypes.VIDEO);

    this._startVideoCallScreen(session.id, opponenetId, context);
  }

  _monitorCalls(BuildContext context, int myId) async {
    try {

      await QB.webrtc.subscribeRTCEvent(QBRTCEventTypes.CALL, (data) {
        print("something happened : $data");
        print(QBRTCEventTypes.CALL);

        QBRTCSession modelData = this._maptoModel(data);
        this._controller.add(modelData);

        /*
        showDialog(context: context, builder: (context) => AlertDialog(
            title: Text("${qbSession.initiatorId} is calling you."),
            content: Text("Do you want to receive?"),
            actions: [
              FlatButton(
                  onPressed: () async {
                    Navigator.of(context).pop();
                    this._controller.add(qbSession);
                    try {
                      Map<String, Object> userInfo = new Map();

                      QBRTCSession obj = await QB.webrtc.accept(qbSession.id, userInfo: userInfo);
                      print("this._startVideoCallScreen");
                      this._startVideoCallScreen(
                          obj.id, obj.initiatorId, context);
                    } on PlatformException catch (e) {
                      print("Call accept exception : ${e.toString()}");
                    }
                  },
                  child: Text("Yes")),
              FlatButton(
                  onPressed: () async {
                    Navigator.of(context).pop();
                    Map<String, Object> userInfo = new Map();

                    try {
                      await QB.webrtc.reject(qbSession.id, userInfo: userInfo);
                    } on PlatformException catch (e) {
                      print("Call reject exception : ${e.toString()}");
                    }
                  },
                  child: Text("No"))
            ]));
        */
      });
    } on PlatformException catch (e) {
      print("QuickBlox new event exception : ${e.toString()}");
    }
  }


  Future<QBRTCSession> acceptCall(String sessionId, BuildContext context) async {
    try {
      Map<String, Object> userInfo = new Map();

      QBRTCSession obj = await QB.webrtc.accept(sessionId, userInfo: userInfo);
      print("this._startVideoCallScreen");
      return obj;
    } on PlatformException catch (e) {
      print("Call accept exception : ${e.toString()}");
    }
  }


  rejectCall(String sessionId) async {
    try {
      Map<String, Object> userInfo = new Map();
      await QB.webrtc.reject(sessionId, userInfo: userInfo);
    } on PlatformException catch (e) {
      print("Call reject exception : ${e.toString()}");
    }
  }


  Future<QBRTCSession> endCall(String sessionId) async {
    Map<String, Object> userInfo = new Map();

    try {
      QBRTCSession session = await QB.webrtc.hangUp(sessionId, userInfo: userInfo);
      return session;
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  QBRTCSession _maptoModel(dynamic data){
    Map<String, Object> map_complete = Map<String, dynamic>.from(data);
    Map<String, Object> map_payload =
    Map<String, dynamic>.from(map_complete["payload"]);
    Map<String, Object> map_session =
    Map<String, dynamic>.from(map_payload["session"]);

    print("this is session dictionary : $map_session");
    QBRTCSession qbSession =
    QBRTCSessionMapper.mapToQBRtcSession(map_session);
    print("qbSession id : ${qbSession.id}");
    return  qbSession;
  }
}
