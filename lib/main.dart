import 'package:quickblox_sdk/settings/module.dart';
import 'package:tele_medi/model/chat.dart';
import 'package:tele_medi/screens/call.dart';
import 'package:tele_medi/screens/chatScren.dart';
import 'package:tele_medi/screens/homeScreen.dart';
import 'package:tele_medi/screens/loginScreen.dart';
import 'package:tele_medi/screens/makeCallScreen.dart';
import 'package:tele_medi/screens/settingsScreen.dart';
import 'package:tele_medi/screens/signUpScreen.dart';
import 'package:tele_medi/screens/stackView.dart';
import 'package:tele_medi/baseClasses/tabbarController.dart';
import 'package:tele_medi/screens/videoCall.dart';
import 'package:tele_medi/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quickblox_sdk/quickblox_sdk.dart';
import 'model/chat.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  try {
    await QB.settings.init(
        AppId, Authorization_key, Authorization_secret, Account_key,
        apiEndpoint: "", chatEndpoint: "");
  } catch (e) {
    print(e.toString());
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [ChangeNotifierProvider.value(value: ChatModelManager())],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Demo',
          theme: ThemeData(
            accentColor: Colors.deepOrange,
            primaryColor: Colors.purple,
          ),
          home: LoginScreen(),
          routes: {
            TabScreen.routeName: (ctx) => TabScreen(),
            HomeScreen.routeName: (ctx) => HomeScreen(),
            CallPage.routeName: (ctx) => CallPage(),
            ChatWindow.routeName: (ctx) => ChatWindow(),
            VideoCallScreen.routeName: (ctx) => VideoCallScreen(),
            MakeCallScreen.routeName: (ctx) => MakeCallScreen(),
            SettingsScreen.routeName: (ctx) => SettingsScreen(),
            SignUpScreen.routeName: (ctx) => SignUpScreen()
          },
        ));
  }
}
