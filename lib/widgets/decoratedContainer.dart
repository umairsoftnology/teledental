import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DecoratedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(margin: EdgeInsets.all(16),
      padding: EdgeInsets.all(16),
      decoration: this.boxDecor());
  }

  BoxDecoration boxDecor() {
    return BoxDecoration(
        color: Colors.white,
        border: Border.all(),
        borderRadius: BorderRadius.all(Radius.circular(6)),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 10, offset: Offset(1, 3))
        ]);
  }
}
