import 'dart:convert';
import 'package:tele_medi/model/album.dart';
import 'package:tele_medi/model/tempClass.dart';
import 'package:tele_medi/utils/constants.dart';
import 'package:http/http.dart' as http;

class WebAPIs {
  static Future<Album> fetchAlbum() async {
    var client = http.Client();
    try {
      var uriResponse =
          await client.read(url);
      return Album.fromJson(json.decode(uriResponse));
    } finally {
      client.close();
    }
  }

  static Future<TempClassParent> fetchTempClassData() async {
    var client = http.Client();
    try {
      var uriResponse = await client.read(url2);
      return TempClassParent.fromJson(json.decode(uriResponse));
    } finally {
      print("finally called");
      client.close();
    }
  }
}

